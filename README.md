# Uberlike APIs

## Usage

1. Clone the repo
2. Run `npm install` to install dependencies
3. Create a .env file in path `./src/configs` to set up environment variable

```
/* example */
./src/config/.env

PORT=8080
DB_URL='mongodb://127.0.0.1:27017/uber-like-api'
PASSWORD_MIN_LENGTH=8
SALT_ROUND=10
JWT_KEY='hellothere'
JWT_TOKEN_LIFE='15m'
ENV='DEV'
```

4. Run `npm start` to start express server for this API.
