const loaders = require('./loaders');
const express = require('express');
const chalk = require('chalk');

/**
 * function to create express server
 */
async function startServer() {
  const app = express();
  const PORT = process.env.PORT;

  await loaders({expressApp: app});

  app.listen(PORT, (err) => {
    if (err) {
      console.log(err);
      return;
    }
    console.log(
        `Your server is ready on PORT ${chalk.bold.italic.yellow(PORT)}`,
    );
  });
}

startServer();
