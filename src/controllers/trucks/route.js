const express = require('express');
const {celebrate} = require('celebrate');
const verifyJWT = require('../middlewares/verifyJWT');
const isRole = require('../middlewares/isRole');

const {
  createTruck,
  getAllTrucks,
  getTruckById,
  updateTruck,
  deleteTruck,
  assignTruckToCurrentUser,
} = require('./services');
const {truckType} = require('./dto');

const route = new express.Router();

route.use(verifyJWT);
// this path is only available for 'DRIVER' role
route.use(isRole('DRIVER'));

route.get('/:id', getTruckById);
route.get('/', getAllTrucks);
route.post('/', celebrate(truckType), createTruck);
route.put('/:id', celebrate(truckType), updateTruck);
route.delete('/:id', deleteTruck);
route.post('/:id/assign', assignTruckToCurrentUser);

module.exports = route;
