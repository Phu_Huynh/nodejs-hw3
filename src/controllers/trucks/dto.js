const {Joi, Segments} = require('celebrate');
const {joiTruck} = require('../../common/joi');

const truckType = {
  [Segments.BODY]: Joi.object().keys({
    type: joiTruck.type,
  }),
};

module.exports = {
  truckType,
};
