const Truck = require('../../models/truck.model');
const {internal} = require('@hapi/boom');

module.exports.getAllTrucks = async (req, res, next) => {
  try {
    const user = res.locals.user;
    await user.populate({path: 'trucks'});
    res.locals.res = {
      statusCode: 200,
      payload: {
        trucks: user.trucks,
      },
    };
    next();
  } catch (err) {
    next(internal(err));
  }
};

module.exports.createTruck = async (req, res, next) => {
  try {
    const {type} = req.body;
    const user = res.locals.user;
    const newTruck = new Truck({created_by: user._id, type});
    await newTruck.save();
    res.locals.res = {
      statusCode: 200,
      payload: {message: 'Truck created successfully'},
    };
    next();
  } catch (err) {
    next(internal(err));
  }
};

module.exports.getTruckById = async (req, res, next) => {
  try {
    const {id} = req.params;
    const truck = await Truck.find({_id: id});
    res.locals.res = {
      statusCode: 200,
      payload: truck,
    };
    next();
  } catch (err) {
    next(internal(err));
  }
};

module.exports.updateTruck = async (req, res, next) => {
  try {
    const {id} = req.params;
    const {type} = req.body;
    const truck = await Truck.findOne({_id: id});
    truck.type = type;
    await truck.save();
    res.locals.res = {
      statusCode: 200,
      payload: {
        message: 'Truck details changed successfully',
      },
    };
    next();
  } catch (err) {
    next(internal(err));
  }
};

module.exports.deleteTruck = async (req, res, next) => {
  try {
    const {id} = req.params;
    await Truck.deleteOne({_id: id});
    res.locals.res = {
      statusCode: 200,
      payload: {
        message: 'Truck deleted successfully',
      },
    };
    next();
  } catch (err) {
    next(internal(err));
  }
};

module.exports.assignTruckToCurrentUser = async (req, res, next) => {
  try {
    const {id} = req.params;
    const user = res.locals.user;
    const truck = await Truck.findOne({_id: id});
    truck.assigned_to = user._id;
    await truck.save();

    res.locals.res = {
      statusCode: 200,
      payload: {
        message: 'Truck assigned successfully',
      },
    };
    next();
  } catch (err) {
    next(internal(err));
  }
};
