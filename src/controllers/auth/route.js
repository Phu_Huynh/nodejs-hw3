const express = require('express');
const {celebrate} = require('celebrate');

const {loginCredentials, createUser} = require('./dto');
const {register, login} = require('./services');

const route = new express.Router();

route.post('/register', celebrate(createUser), register);

route.post('/login', celebrate(loginCredentials), login);

module.exports = route;
