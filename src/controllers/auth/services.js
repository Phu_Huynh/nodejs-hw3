const User = require('../../models/user.model');
const {internal} = require('@hapi/boom');
const {generateJWTToken} = require('../../common/jwt');

module.exports.register = async (req, res, next) => {
  try {
    const {email, password, role} = req.body;

    const user = new User({email, password, role});
    await user.save();

    res.locals.res = {
      statusCode: 200,
      payload: {
        message: 'Profile created successfully',
      },
    };
    next();
  } catch (err) {
    next(internal(err));
  }
};

module.exports.login = async (req, res, next) => {
  try {
    const {email, password} = req.body;

    let user = {};
    user = await await User.findByCredentials({email, password});

    const jwtToken = generateJWTToken({_id: user._id});
    res.locals.res = {
      statusCode: 200,
      payload: {jwt_token: jwtToken},
    };
    next();
  } catch (err) {
    return next(internal(err));
  }
};
