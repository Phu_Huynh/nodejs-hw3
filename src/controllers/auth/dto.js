const {Joi, Segments} = require('celebrate');
const {joiUser} = require('../../common/joi');

const createUser = {
  [Segments.BODY]: Joi.object().keys({
    email: joiUser.email,
    password: joiUser.password,
    role: joiUser.role,
  }),
};

const loginCredentials = {
  [Segments.BODY]: Joi.object().keys({
    email: joiUser.email,
    password: joiUser.password,
  }),
};

module.exports = {
  createUser,
  loginCredentials,
};
