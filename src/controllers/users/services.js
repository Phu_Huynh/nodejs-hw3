const {internal} = require('@hapi/boom');
const bcrypt = require('bcrypt');

const User = require('../../models/user.model');

module.exports.getUser = async (req, res, next) => {
  try {
    res.locals.res = {
      statusCode: 200,
      payload: res.locals.user,
    };
    next();
  } catch (err) {
    next(internal(err.message));
  }
};

module.exports.changePassword = async (req, res, next) => {
  try {
    const {oldPassword, newPassword} = req.body;
    const user = res.locals.user;

    const isPasswordMatch = await bcrypt.compare(oldPassword, user.password);

    if (!isPasswordMatch) {
      next(badRequest('oldPassword does not match'));
      return;
    }

    user.password = newPassword;
    await user.save();
    res.locals.res = {
      statusCode: 200,
      payload: {message: 'Success'},
    };
    next();
  } catch (err) {
    next(internal(err.message));
  }
};

module.exports.deleteUser = async (req, res, next) => {
  try {
    const user = res.locals.user;
    await User.deleteOne({_id: user._id});
    res.locals.res = {
      statusCode: 200,
      payload: {message: 'Success'},
    };
    next();
  } catch (err) {
    next(internal(err.message));
  }
};
