const express = require('express');
const {celebrate} = require('celebrate');
const verifyJWT = require('../middlewares/verifyJWT');
const {updatePassword} = require('./dto');
const {getUser, changePassword} = require('./services');

const route = new express.Router();

route.use(verifyJWT);

// GET requesting user profile
route.get('/me', getUser);

// CHANGE user password
route.patch('/me', celebrate(updatePassword), changePassword);

// DELETE user's profile
route.delete('/me');

module.exports = route;
