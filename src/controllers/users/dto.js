const {Joi, Segments} = require('celebrate');
const {joiUser} = require('../../common/joi');

const updatePassword = {
  [Segments.BODY]: Joi.object().keys({
    oldPassword: joiUser.password,
    newPassword: joiUser.password,
  }),
};

module.exports = {
  updatePassword,
};
