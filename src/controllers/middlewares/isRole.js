const {internal, badRequest} = require('@hapi/boom');
const {joiUser} = require('../../common/joi');

module.exports = (role) => {
  return async (req, res, next) => {
    try {
      await joiUser.role.validateAsync(role);
      const user = res.locals.user;
      if (user.role.toLowerCase() === role.toLowerCase()) {
        next();
      } else {
        next(badRequest(`This endpoint only available for ${role}`));
      }
    } catch (err) {
      next(internal(err));
    }
  };
};
