const User = require('../../models/user.model');

const {badRequest} = require('@hapi/boom');

const {checkJWTToken} = require('../../common/jwt');

const verifyJWT = async (req, res, next) => {
  try {
    const token = req.header('Authorization').replace('Bearer ', '');
    const decoded = checkJWTToken(token);

    const user = await User.findOne({_id: decoded._id});

    if (!user) {
      throw new Error('The user does not exist!');
    }

    res.locals.token = token;
    res.locals.user = user;
    next();
  } catch (err) {
    next(badRequest('Invalid jwt_token. Please login!'));
  }
};

module.exports = verifyJWT;
