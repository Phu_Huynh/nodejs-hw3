const express = require('express');
const {celebrate} = require('celebrate');
const verifyJWT = require('../middlewares/verifyJWT');
const isRole = require('../middlewares/isRole');

const {createLoad} = require('./dto');
const {addNewLoad, getLoadsWithRole, getActiveLoads} = require('./services');

const route = new express.Router();

route.use(verifyJWT);

route.get('/', getLoadsWithRole);
route.post('/', isRole('SHIPPER'), celebrate(createLoad), addNewLoad);
route.get('/active', isRole('DRIVER'), getActiveLoads);

module.exports = route;
