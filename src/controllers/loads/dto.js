const {Joi, Segments} = require('celebrate');
const {joiLoad} = require('../../common/joi');

const createLoad = {
  [Segments.BODY]: Joi.object().keys({
    name: joiLoad.name,
    payload: joiLoad.payload,
    pickup_address: joiLoad.pickup_address,
    delivery_address: joiLoad.delivery_address,
    dimensions: joiLoad.dimensions,
  }),
};

module.exports = {
  createLoad,
};
