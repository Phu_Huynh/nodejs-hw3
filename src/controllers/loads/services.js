const {internal} = require('@hapi/boom');
const Load = require('../../models/load.model');

module.exports.getLoadsWithRole = async (req, res, next) => {
  try {
    let {status, limit, offset} = req.query;
    offset = !!parseInt(offset) ? parseInt(offset) : 0;
    limit = !!parseInt(limit) ? parseInt(limit) : 0;

    const filter = !!status ? {status: status.toUpperCase()} : {};

    const loadList = await Load.find(filter, null, {
      skip: offset,
      limit,
    });
    res.locals.res = {
      statusCode: 200,
      payload: {loads: loadList},
    };
    next();
  } catch (err) {
    console.log(err);
    next(internal(err));
  }
};

module.exports.addNewLoad = async (req, res, next) => {
  try {
    const user = res.locals.user;
    console.log(req.body);
    /* eslint camelcase: "off"*/
    const {name, payload, pickup_address, delivery_address, dimensions} =
      req.body;
    const newLoad = new Load({
      created_by: user._id,
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    });

    await newLoad.save();

    res.locals.res = {
      statusCode: 200,
      payload: {
        message: 'Load created successfully',
      },
    };
    next();
  } catch (err) {
    next(internal(err));
  }
};

module.exports.getActiveLoads = async (req, res, next) => {};
