const express = require('express');

const authControllers = require('./auth/route');
const usersControllers = require('./users/route');
const trucksControllers = require('./trucks/route');
const loadsControllers = require('./loads/route');

const route = new express.Router();
route.use('/api/auth', authControllers);
route.use('/api/users', usersControllers);
route.use('/api/trucks', trucksControllers);
route.use('/api/loads', loadsControllers);

module.exports = route;
