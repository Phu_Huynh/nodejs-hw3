const jwt = require('jsonwebtoken');

const KEY = !!process.env.JWT_KEY ? process.env.JWT_KEY : 'default';
const TOKEN_LIFE_TIME = !!process.env.JWT_TOKEN_LIFE ?
  process.env.JWT_TOKEN_LIFE :
  '30m';
/**
 *
 * @param {Object} payload
 * @return {string} jwt_token
 */
function generateJWTToken(payload) {
  const token = jwt.sign(payload, KEY, {
    expiresIn: TOKEN_LIFE_TIME,
  });
  return token;
}

/**
 *
 * @param {string} jwtToken
 * @return {Object} decoded payload
 */
function checkJWTToken(jwtToken) {
  const decoded = jwt.verify(jwtToken, KEY);
  return decoded;
}

module.exports = {
  generateJWTToken,
  checkJWTToken,
};
