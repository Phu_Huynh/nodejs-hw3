const {Joi} = require('celebrate');

const PW_MINLEN = !!parseInt(process.env.PASSWORD_MIN_LENGTH) ?
  parseInt(process.env.PASSWORD_MIN_LENGTH) :
  8;

const joiConstants = {
  userRoles: ['SHIPPER', 'DRIVER'],
  passwordRequirements: `Password requirements:
- at least ${PW_MINLEN} characters
- must contain at least 1 uppercase letter, 1 lowercase letter, and 1 number
- Can contain special characters`,
  truckTypes: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  truckStatuses: ['OL', 'IS'],
  loadStatuses: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  loadStates: [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery',
  ],
};

const joiUser = {
  email: Joi.string().email({
    minDomainSegments: 2,
  }),
  password: Joi.string().pattern(
      new RegExp(`^(?=.*\\d)(?=.*[a-z])(?=.*[a-zA-Z]).{${PW_MINLEN},}$`),
  ),
  role: Joi.string().valid(...joiConstants.userRoles),
};

const joiTruck = {
  type: Joi.string().valid(...joiConstants.truckTypes),
  status: Joi.string().valid(...joiConstants.truckStatuses),
};

const joiLoad = {
  status: Joi.string().valid(...joiConstants.loadStatuses),
  state: Joi.string().valid(...joiConstants.loadStates),
  name: Joi.string(),
  payload: Joi.number().integer(),
  pickup_address: Joi.string(),
  delivery_address: Joi.string(),
  dimensions: Joi.object({
    width: Joi.number().integer(),
    length: Joi.number().integer(),
    height: Joi.number().integer(),
  }),
};

module.exports = {
  joiUser,
  joiConstants,
  joiTruck,
  joiLoad,
};
