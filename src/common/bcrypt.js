const bcrypt = require('bcrypt');

const SALT_ROUND = !!parseInt(process.env.SALT_ROUND) ?
  parseInt(process.env.SALT_ROUND) :
  10;

/**
   *
   * @param {string} _string
   * @return {string | undefined} -- hashed string
   */
async function hashString(_string) {
  try {
    const hash = await bcrypt.hash(_string, SALT_ROUND);
    return hash;
  } catch (err) {
    console.error(err);
    return undefined;
  }
}

/**
 *
 * @param {string} _string -- normal string
 * @param {string} _hash -- hashed string
 * @return {boolean | undefined}
 */
async function checkHashString(_string, _hash) {
  try {
    const isMatch = await bcrypt.compare(_string, _hash);
    return isMatch;
  } catch (err) {
    console.error(err);
    return undefined;
  }
}

module.exports = {
  hashString,
  checkHashString,
};
