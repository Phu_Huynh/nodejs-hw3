// path option is absolute path from package.json
require('dotenv').config({path: 'src/configs/.env'});
const expressLoader = require('./express');
const mongooseLoader = require('./mongoose');

module.exports = async ({expressApp}) => {
  await mongooseLoader();
  console.log('MongoDB Initialized');

  await expressLoader({app: expressApp});
  console.log('Express Initialized');

  // ... more loaders can be here
};
