const express = require('express');
const path = require('path');
const fs = require('fs');
const morgan = require('morgan');
const cors = require('cors');
const {badRequest} = require('@hapi/boom');
const {errors} = require('celebrate');
const routeControllers = require('../controllers');

module.exports = async ({app}) => {
  app.use(
      cors({
        origin: 'https://editor.swagger.io',
      }),
  );
  app.use(express.json());
  if (process.env.ENV === 'DEV') {
    app.use(require('morgan')('dev'));
  }
  app.use(express.urlencoded({extended: false}));

  // Create a write stream (in append mode)
  const accessLogStream = fs.createWriteStream(
      path.join(...__dirname.split(/[\\/]/).slice(0, this.length - 2), '.log'),
      {
        flags: 'a',
      },
  );
  // Setup the logger with morgan; Apache style
  app.use(morgan('combined', {stream: accessLogStream}));

  app.use('/', routeControllers);

  // Response send handler
  // attach data to res.locals.res = {statusCode, payload}
  app.use((req, res, next) => {
    if (res.locals.res) {
      const {statusCode, payload} = res.locals.res;
      return res.status(statusCode).send(payload);
    } else {
      next();
    }
  });

  // Return error on any wrong path request
  app.all('*', (req, res, next) => {
    return next(badRequest('This route does not exist'));
  });

  // Celebrate error handler
  app.use(errors());

  // Error handler middleware at the end
  // with @hapi/boom 's boom object
  app.use((err, req, res, next) => {
    if (err.isBoom && err.output) {
      return res.status(err.output.statusCode).send(err.output.payload);
    }
    if (err) {
      return res.status(500).send({message: err.message});
    }
  });

  // Return the express app
  return app;
};
