const mongoose = require('mongoose');
const connection =
  process.env.DB_URL || 'mongodb://127.0.0.1:27017/uber-like-api';

module.exports = async () => {
  try {
    await mongoose.connect(connection);
    return true;
  } catch (err) {
    console.error('Fail to connect to mongodb with error:', err);
  }
};
