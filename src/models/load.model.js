const mongoose = require('mongoose');
const {Schema, ObjectId} = mongoose;
const {joiConstants} = require('../common/joi');

const loadSchema = new Schema(
    {
      created_by: {
        type: ObjectId,
        required: true,
        ref: 'User',
      },
      assigned_to: {
        type: ObjectId,
        ref: 'User',
      },
      status: {
        type: String,
        uppercase: true,
        enum: {
          values: joiConstants.loadStatuses,
          message: `{VALUE} is invalid. Only [${joiConstants.loadStatuses}]`,
        },
        default: joiConstants.loadStatuses[0],
      },
      state: {
        type: String,
        enum: {
          values: joiConstants.loadStates,
          message: `{VALUE} is invalid. Only [${joiConstants.loadStates}]`,
          default: joiConstants.loadStates[0],
        },
      },
      name: {
        type: String,
        required: true,
      },
      payload: {
        type: Number,
        required: true,
      },
      pickup_address: {
        type: String,
        required: true,
      },
      delivery_address: {
        type: String,
        require: true,
      },
      dimensions: {
        width: {
          type: Number,
          required: true,
        },
        length: {
          type: Number,
          required: true,
        },
        height: {
          type: Number,
          required: true,
        },
      },
      logs: [
        {
          message: {
            type: String,
          },
          time: {
            type: Date,
          },
        },
      ],
    },

    {
    // Schema option to enable mongoose manage createAt and updateAt
      timestamps: true,
    },
);

// Modify .toJSON() method on Note model instance
loadSchema.methods.toJSON = function() {
  const load = this;
  const loadObject = load.toObject();

  loadObject.created_date = loadObject.createdAt;

  delete loadObject.createdAt;
  delete loadObject.updatedAt;
  delete loadObject.__v;

  return loadObject;
};

const Load = mongoose.model('Load', loadSchema);

module.exports = Load;
