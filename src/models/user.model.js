const mongoose = require('mongoose');
const {Schema} = mongoose;

const {hashString, checkHashString} = require('../common/bcrypt');
const {joiUser, joiConstants} = require('../common/joi');

const userSchema = new Schema(
    {
      email: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        validate: function() {
          const email = this.email;
          const res = joiUser.email.validate(email);
          if (res.error) {
            throw new Error('Invalid email address!');
          }
          return true;
        },
      },
      password: {
        type: String,
        required: true,
        trim: true,
        validate: function() {
          const password = this.password;
          const res = joiUser.password.validate(password);
          if (res.error) {
            throw new Error(joiConstants.passwordRequirements);
          }
          return true;
        },
      },
      role: {
        type: String,
        required: true,
        uppercase: true,
        enum: {
          values: joiConstants.userRoles,
          message: `{VALUE} is invalid. Only [${joiConstants.userRoles}]`,
        },
      },
    },
    {
    // Schema option to enable mongoose manage createAt and updateAt
      timestamps: true,
    },
);

// call user.populate(<<virtual field>>).execPopulate() to get the ref record
userSchema.virtual('trucks', {
  ref: 'Truck',
  localField: '_id',
  foreignField: 'created_by',
});

userSchema.virtual('loads', {
  ref: 'Load',
  localField: '_id',
  foreignField: 'created_by',
});

// Hash the plain text password before saving to mongodb
userSchema.pre('save', async function(next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await hashString(user.password);
  }
  next();
});

// .methods is used to modify inherit methods
userSchema.methods.toJSON = function() {
  const user = this;
  const userObject = user.toObject();

  userObject.created_date = userObject.createdAt;

  delete userObject.password;
  delete userObject.createdAt;
  delete userObject.updatedAt;
  delete userObject.__v;

  return userObject;
};

// .statics used to attach static method to Model created from this schema
userSchema.statics.findByCredentials = async function({email, password}) {
  const user = await this.findOne({email});
  if (!user) {
    throw new Error('this email does not exist in db');
  }
  const isMatch = await checkHashString(password, user.password);
  if (!isMatch) {
    throw new Error('password does not match');
  }
  return user;
};

const User = mongoose.model('User', userSchema);

module.exports = User;
