const mongoose = require('mongoose');
const {Schema, ObjectId} = mongoose;
const {joiConstants} = require('../common/joi');

const truckSchema = new Schema(
    {
      created_by: {
        type: ObjectId,
        required: true,
        ref: 'User',
      },
      assigned_to: {
        type: ObjectId,
        ref: 'User',
      },
      type: {
        type: String,
        required: true,
        uppercase: true,
        enum: {
          values: joiConstants.truckTypes,
          message: `{VALUE} is invalid. Only [${joiConstants.truckTypes}]`,
        },
      },
      status: {
        type: String,
        uppercase: true,
        enum: {
          values: joiConstants.truckStatuses,
          message: `{VALUE} is invalid. Only [${joiConstants.truckStatuses}]`,
        },
        default: joiConstants.truckStatuses[0],
      },
    },

    {
    // Schema option to enable mongoose manage createAt and updateAt
      timestamps: true,
    },
);

// Modify .toJSON() method on Note model instance
truckSchema.methods.toJSON = function() {
  const truck = this;
  const truckObject = truck.toObject();

  truckObject.created_date = truckObject.createdAt;

  delete truckObject.createdAt;
  delete truckObject.updatedAt;
  delete truckObject.__v;

  return truckObject;
};

const Truck = mongoose.model('Truck', truckSchema);

module.exports = Truck;
